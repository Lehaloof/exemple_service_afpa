package example.dt;

import example.dt.mappers.PersonneMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(PersonneMapper.class)
public class DtApplication {

	public static void main(String[] args) {
		SpringApplication.run(DtApplication.class, args);
	}

}
