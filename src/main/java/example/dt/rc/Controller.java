package example.dt.rc;

import example.dt.entities.Personne;
import example.dt.mappers.PersonneMapper;
import example.dt.repositories.PersonneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@CrossOrigin("*")
@RequestMapping("/api")
@RestController
public class Controller {

    @Autowired
    private PersonneRepository personneRepository;

    @Autowired
    @Qualifier("personneMapper")
    private PersonneMapper mapper;

    @GetMapping("/personne")
    public List<Personne> getAllPersonnes() {
        return personneRepository.findAll();
    }

    @RequestMapping(path = "/personne/{id}", method = RequestMethod.GET)
    public Personne getPersonneById(@PathVariable("id") Integer id) {
        return personneRepository.findPersonneById(id);
    }

    @PostMapping("/personne")
    public void postPersonne(@RequestBody Personne personne) {
        personneRepository.save(personne);
    }

    @PutMapping("/personne")
    public void putPersonne(@RequestBody Personne personne) {
        personneRepository.save(personne);
    }

    @DeleteMapping("/personne/{id}")
    public void deletePersonneById(@PathVariable("id") Integer id) {
        personneRepository.deleteById(id);
    }

    @GetMapping("/personne/config")
    public Collection<Personne> getPersonnesInConfig() {
        return mapper.getMapper().values();
    }

}
