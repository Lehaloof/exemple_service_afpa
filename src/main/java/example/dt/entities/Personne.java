package example.dt.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Personne {

    @Id
    @Column(name = "ID_PERSONNE")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idPersonne;

    @Column(name = "NOM")
    private String nom;

    @Column(name = "PRENOM")
    private List<String> prenom;

    private int age;

    public String afficherPersonne() {
        return "Nom : " + this.nom + "\nPrénom : " + afficherPrenom() + "\nAge : " + this.age + "ans";
    }

    private String afficherPrenom() {
        String strPrenom = "";
        for (int i = 0; i < this.prenom.size(); i++) {
            strPrenom += this.prenom.get(i) + " ";
        }

        return strPrenom;
    }

}
