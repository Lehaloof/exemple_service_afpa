package example.dt.repositories;

import example.dt.entities.Personne;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PersonneRepository extends JpaRepository<Personne, Integer> {

    @Query("select p from Personne p where p.idPersonne = :id")
    public Personne findPersonneById(@Param("id") Integer id);

}
