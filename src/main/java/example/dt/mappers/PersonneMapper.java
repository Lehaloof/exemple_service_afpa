package example.dt.mappers;

import example.dt.entities.Personne;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Data
@Component("personneMapper")
@ConfigurationProperties(prefix = "personne")
public class PersonneMapper {

    private Map<String, Personne> mapper = new HashMap<>();


}
