package example.dt.entitiesTest;

import example.dt.entities.Personne;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class PersonneTest {

    @Test
    void testPassant() {
        /*
         * THEN
         */
        assertEquals("Nom : Lescombes\nPrénom : Loic Jérôme \nAge : 23ans", getAffichage());
    }

    @Test
    void TestNonPassant() {
        /*
         * THEN
         */
        assertNotEquals("Nom: Lescombes\nPrénom: Loic Jérôme \nAge: 23ans", getAffichage());
    }

    String getAffichage() {
        /*
         * GIVEN
         */
        List<String> prenom = new ArrayList<>();
        prenom.add("Loic");
        prenom.add("Jérôme");
        Personne personne = new Personne(0, "Lescombes", prenom, 23);

        /*
         * WHEN
         */
        return personne.afficherPersonne();
    }

}
